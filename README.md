# Valmeth

R script for statistical validation of analytical procedures

* Adapted from *Harmonization of strategies for the validation of quantitative analytical procedures - A SFSTP proposal - Part III* by Ph. Hubert et al., 2007

* 2 model types : simple linear regression and quadratic regression
